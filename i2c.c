// TI File $Revision: /main/2 $
// Checkin $Date: December 16, 2010   11:15:00 $
//###########################################################################
//
// FILE:    DSP2802x_I2C.c
//
// TITLE:    DSP2802x I2C Initialization & Support Functions.
//
//###########################################################################
// $TI Release: LaunchPad f2802x Support Library v100 $
// $Release Date: Wed Jul 25 10:45:39 CDT 2012 $
//###########################################################################

#include "sw/drivers/gpio/src/32b/f28x/f2802x/gpio.h"
#include "sw/drivers/pie/src/32b/f28x/f2802x/pie.h"
#include "sw/drivers/clk/src/32b/f28x/f2802x/clk.h"
#include "sw/drivers/cpu/src/32b/f28x/f2802x/cpu.h"

#include "F2802x_Device.h"     // Headerfile Include File
#include "F2802x_I2C.h"     // Headerfile Include File


#include "hal_obj.h"
#include "user.h"

int i2c_cmd_handler(unsigned char* data, size_t bytes);

//--------------------------------------------
// Defines
//--------------------------------------------

// Error Messages
#define I2C_ERROR               0xFFFF
#define I2C_ARB_LOST_ERROR      0x0001
#define I2C_NACK_ERROR          0x0002
#define I2C_BUS_BUSY_ERROR      0x1000
#define I2C_STP_NOT_READY_ERROR 0x5555
#define I2C_NO_FLAGS            0xAAAA
#define I2C_SUCCESS             0x0000

// Clear Status Flags
#define I2C_CLR_AL_BIT          0x0001
#define I2C_CLR_NACK_BIT        0x0002
#define I2C_CLR_ARDY_BIT        0x0004
#define I2C_CLR_RRDY_BIT        0x0008
#define I2C_CLR_SCD_BIT         0x0020

// Interrupt Source Messages
#define I2C_NO_ISRC             0x0000
#define I2C_ARB_ISRC            0x0001
#define I2C_NACK_ISRC           0x0002
#define I2C_ARDY_ISRC           0x0003
#define I2C_RX_ISRC             0x0004
#define I2C_TX_ISRC             0x0005
#define I2C_SCD_ISRC            0x0006
#define I2C_AAS_ISRC            0x0007

// I2CMSG structure defines
#define I2C_NO_STOP  0
#define I2C_YES_STOP 1
#define I2C_RECEIVE  0
#define I2C_TRANSMIT 1
#define I2C_MAX_BUFFER_SIZE 4

// I2C Slave State defines
#define I2C_NOTSLAVE      0
#define I2C_ADDR_AS_SLAVE 1
#define I2C_ST_MSG_READY  2

// I2C Slave Receiver messages defines
#define I2C_SND_MSG1 1
#define I2C_SND_MSG2 2

// I2C State defines
#define I2C_IDLE               0
#define I2C_SLAVE_RECEIVER     1
#define I2C_SLAVE_TRANSMITTER  2
#define I2C_MASTER_RECEIVER    3
#define I2C_MASTER_TRANSMITTER 4

#pragma DATA_SECTION(I2caRegs,"I2caRegsFile");
volatile struct I2C_REGS I2caRegs;

static unsigned char buffer[128] = {0};
static size_t avaliable = 0;
static size_t transmit = 0;

static PIE_Handle pieHandle;

interrupt void i2c_int1a_isr(void);

//---------------------------------------------------------------------------
// Example: InitI2CGpio:
//---------------------------------------------------------------------------
// This function initializes GPIO pins to function as I2C pins
//
// Each GPIO pin can be configured as a GPIO pin or up to 3 different
// peripheral functional pins. By default all pins come up as GPIO
// inputs after reset.
//
// Caution:
// Only one GPIO pin should be enabled for SDAA operation.
// Only one GPIO pin shoudl be enabled for SCLA operation.
// Comment out other unwanted lines.

void InitI2CGpio(GPIO_Handle handle)
{
   EALLOW;

   GPIO_setDirection(handle, GPIO_Number_32, GPIO_Direction_Input);
   GPIO_setDirection(handle, GPIO_Number_33, GPIO_Direction_Input);

/* Enable internal pull-up for the selected pins */
// Pull-ups can be enabled or disabled disabled by the user.
// This will enable the pullups for the specified pins.
// Comment out other unwanted lines.
    GPIO_setPullUp(handle, GPIO_Number_32, GPIO_PullUp_Enable);
    GPIO_setPullUp(handle, GPIO_Number_33, GPIO_PullUp_Enable);

/* Set qualification for selected pins to asynch only */
// This will select asynch (no qualification) for the selected pins.
// Comment out other unwanted lines.
    GPIO_setQualification(handle, GPIO_Number_32, GPIO_Qual_ASync);
    GPIO_setQualification(handle, GPIO_Number_33, GPIO_Qual_ASync);

/* Configure I2C pins using GPIO regs*/
// This specifies which of the possible GPIO pins will be I2C functional pins.
// Comment out other unwanted lines.
    GPIO_setMode(handle, GPIO_Number_32, GPIO_32_Mode_SDAA);
    GPIO_setMode(handle, GPIO_Number_33, GPIO_33_Mode_SCLA);

    EDIS;
}

//---------------------------------------------------------------------------
// InitI2C:
//---------------------------------------------------------------------------
// This function initializes the I2C to a known state.
//
void InitI2C(HAL_Handle handle)
{
    HAL_Obj *obj = (HAL_Obj *)handle;

    /* Enable the I2C clock */
    CLK_enableI2cClock(obj->clkHandle);

    InitI2CGpio(obj->gpioHandle);

    pieHandle = obj->pieHandle;

    ENABLE_PROTECTED_REGISTER_WRITE_MODE;

    I2caRegs.I2CMDR.bit.IRS = 0;

    I2caRegs.I2COAR = 0;

    I2caRegs.I2CPSC.all = 6;       // Prescaler - need 7-12 Mhz on module clk

    I2caRegs.I2CCLKL = 10;           // NOTE: must be non zero
    I2caRegs.I2CCLKH = 5;            // NOTE: must be non zero
    I2caRegs.I2CCNT = 1;
    I2caRegs.I2CIER.all = 0x24;      // Enable SCD & ARDY interrupts

    I2caRegs.I2CSTR.bit.RRDY = 1; // Clear flag
    /* Enable interrupts */
    //I2caRegs.I2CIER.all = 0x2F;
    I2caRegs.I2CIER.all = 0;
    I2caRegs.I2CIER.bit.AAS = 1;

    I2caRegs.I2CEMDR.bit.BCM = 1;
    I2caRegs.I2CMDR.all = 0x0020; // refer i2c datasheet
    I2caRegs.I2CMDR.bit.MST = 0;

    I2caRegs.I2COAR = 0x1B;

    I2caRegs.I2CMDR.bit.IRS = 1;
    I2caRegs.I2CSTR.all = 0xFFFFU;

    DISABLE_PROTECTED_REGISTER_WRITE_MODE;

    PIE_registerPieIntHandler(pieHandle, PIE_GroupNumber_8, PIE_SubGroupNumber_1, &i2c_int1a_isr);
    PIE_clearInt(pieHandle, PIE_GroupNumber_8);
    PIE_enableInt(pieHandle, PIE_GroupNumber_8, PIE_InterruptSource_I2CA1);
    // enable the cpu interrupt for ADC interrupts
    CPU_enableInt(obj->cpuHandle, CPU_IntNumber_8);
}

void _handle_rx_byte(void)
{
    int result;

    buffer[avaliable++] = I2caRegs.I2CDRR & 0xFF;

    result = i2c_cmd_handler(buffer, avaliable);

    if(result < 0) {
        /* We have a problem, enable NACK and send a zero */
        I2caRegs.I2CMDR.bit.NACKMOD = 1;
    } else if (result > 0){
        /* We need to transmit N bytes of data from the buffer */
        avaliable = result & 0xFFFF;
        transmit = 0;

        /* Enable the Transmit ready interrupt */
        I2caRegs.I2CSTR.bit.XRDY = 1;
    }
}

#ifdef FLASH
#pragma CODE_SECTION(i2c_int1a_isr,"ramfuncs");
#endif

interrupt void i2c_int1a_isr(void)
{
    unsigned short source;
    source = I2caRegs.I2CISRC.all;

    switch(source) {
    case I2C_RX_ISRC:
        /* we got some data bro! */
        _handle_rx_byte();
        break;

    case I2C_TX_ISRC:
        I2caRegs.I2CDXR = (transmit < avaliable)
                ? buffer[transmit++]
                : 0;

        /* Done transmitting, disable this */
        if(transmit == avaliable) {
            I2caRegs.I2CSTR.bit.XRDY = 0;
            transmit = 0;
            avaliable = 0;
        }

        break;

    case I2C_NACK_ISRC:
        /* we sent a NACK? */
        I2caRegs.I2CMDR.bit.NACKMOD = 0;
        break;

    case I2C_AAS_ISRC:
        if(!transmit) avaliable = 0;
        /* Start the transaction */
        I2caRegs.I2CSTR.bit.AAS = 1;
        break;
    }

    PIE_clearInt(pieHandle, PIE_GroupNumber_8);
}

//===========================================================================
// End of file.
//===========================================================================
